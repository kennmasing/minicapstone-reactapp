import React, { Fragment, useState, useEffect } from 'react';
import { Button, Container, Row, Col } from 'reactstrap';
import { Link, Redirect } from 'react-router-dom';
import axios from 'axios';
import Swal from 'sweetalert2';

//NESTING PAGES
import HappyDogProfileForm from '../forms/HappyDogProfileForm';
// import HappyDogsTable from '../tables/HappyDogsTable';
// import MemberModal from '../modals/MemberModal';

const HappyDogsPage = (props) => {

  const [happyDogData, setHappyDogData] = useState({
    token: props.token,
    dog: {}
  })

  const { dog } = happyDogData;

    const url = "http://localhost:8000"

  const getDog = async () => {
    try {
      const res = await axios.get(`${url}/dogs/${props.match.params.id}`)

      console.log("DOGS RESPONSE", res)

      return setHappyDogData({
        dog: res.data
      })
      console.log("DOGS DATA", dog)
    } catch(e) {
      console.log(e.response)
          //SWAL
          Swal.fire({
            title: "Error",
            text: "No dogs found",
            icon: "error",
            showConfirmationButton: false,
            timer: 3000
          })
    }
  }

  useEffect(() => {
    getDog()
  }, [setHappyDogData])

  const updateDog = async (id, updates) => {
    console.log("UPDATE DOG", id, updates)

    try {
        const config = {
          headers: {
            'Content-Type' : 'application/json'
          }
        }

        const body = JSON.stringify(updates);

        const res = await axios.patch(`${url}/dogs/${id}`, body, config)

        // setModalData({
        //   ...modalData,
        //   modal: !modal
        // })

        setHappyDogData({
          ...happyDogData,
          dog: res.data
        })

        getDog()
        //SWAL

    } catch(e) {
      console.log(e)
    }
  }

  return (
  	 <Fragment>
      <Container>
        <Row className="mb-4">
          <Col>
            <h1 className="text-black mt-5"> HAPPY DOGS </h1>
          </Col>
        </Row>      
        <Row>
          <Col md="4" className="border rounded">
          	<HappyDogProfileForm dog={dog} updateDog={updateDog}/>
          </Col>
          <Col md="8" className="">
          </Col>
        </Row>
      </Container>
    </Fragment>
  );
}

export default HappyDogsPage;