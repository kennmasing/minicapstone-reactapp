import React, { Fragment, useState, useEffect } from 'react';
import {
	Container, Row, Col, Button, Form, FormGroup,
	Card, CardImg, CardText, CardBody, CardLink, CardTitle, CardSubtitle
} from 'reactstrap';
import StarRating from '../components/StarRating';
import axios from 'axios';
import Swal from 'sweetalert2';

import HappyCard from '../cards/HappyCard';
import HappyModal  from '../modals/HappyModal';

const HappyPage = (props) => {

	const [dogs, setDogs] = useState([])

  	const url = "http://localhost:8000"

	const getDogs = async () => {
		try {
			const res = await axios.get(`${url}/dogs`)

			console.log("DOGS RESPONSE", res)

			return setDogs(res.data)
			console.log("DOGS DATA", dogs)
		} catch(e) {
			console.log(e.response)
	      	//SWAL
		      Swal.fire({
		        title: "Error",
		        text: "No dogs found",
		        icon: "error",
		        showConfirmationButton: false,
		        timer: 3000
		      })
		}
	}

	useEffect(() => {
		getDogs()
	}, [setDogs])

	//RATE MODAL
	//USESTATE
	const [modalData,  setModalData] = useState({
		modal: false,
		dog: {}
	})

	const { modal, dog } = modalData;

	//TOGGLE FUNCTION
	const toggle = async (id) => {
		console.log(id);
		// alert(id);

		try {	
			if(typeof id === "string") {
				const res = await axios.get(`${url}/dogs/${id}`)

				return setModalData({
					modal: !modal,
					dog: res.data
				})
				console.log("GET DOG", res)
			}
			setModalData({
				...modalData,
				modal: !modal
			})
		} catch(e) {
			console.log(e)
		}
	}

	//USE EFFECT
	useEffect(() => {

	}, [toggle])

	//UPDATE RATE
	const updateDog = async (id, ratingValue) => {

		try {
			const config = {
				headers: {
					"Content-Type" : "application/json"
				}
			}

			const body = JSON.stringify(ratingValue);

			const res = await axios.patch(`${url}/dogs/${id}`, body, config)

			setModalData({
				...modalData,
				modal: !modal
			})

			getDogs()

		} catch(e) {
			console.log(e)
		}
	}
	
	return (
		<Fragment>
			<div className="bg-home02 row no-gutters">
				<row className="">
					<Col md="4" className="offset-md-4 mt-5">
						<HappyCard dogs={dogs} toggle={toggle}/>
					</Col>
				</row>

			</div>
			<HappyModal modal={modal} toggle={toggle} dog={dog} updateDog={updateDog} />
		</Fragment>
	)
}

export default HappyPage;