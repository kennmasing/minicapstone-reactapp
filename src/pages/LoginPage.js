import React from 'react';
import { Container, Row, Col } from 'reactstrap';

import LoginForm from '../forms/LoginForm';
// import CarouselSlider from '../components/CarouselSlider';
import Image01 from '../images/slider/puppies01.jpg';

const LoginPage = (props) => {
	return (
		<div className="d-flex container-fluid max-height row no-gutters">
			<Col md="7" className="container-fluid">
					<div className="img-overlay">
							<p className="text-white"><i class="fas fa-dog"></i>   &nbsp;i gib you lab</p>
							<p className="text-white"><i class="far fa-star"></i>   &nbsp;you gib me lab</p>
							<p className="text-white"><i class="fas fa-donate"></i>   &nbsp;halp other puppers</p>
					</div>
					<img src={Image01} className="img-size"/>
			</Col>
			<Col col="5" className="container-fluid mt-5 row no-gutters d-flex align-items-center mt-n5">
					<Col md="10" className="offset-md-1">
						<h1 className="ml-2">Login</h1>
						<LoginForm/>
					</Col>
			</Col>
		</div>
	)
}

export default LoginPage;