import React, { Fragment, useState, useEffect } from 'react';
import {
	Container, Row, Col, Button,
	Card, CardImg, CardText, CardBody, CardLink, CardTitle, CardSubtitle
} from 'reactstrap';
import { Link, Redirect } from 'react-router-dom';


import RegisterPage from './RegisterPage';
// import CarouselSlider from '../components/CarouselSlider';
import Image01 from '../images/slider/puppies01.jpg';

const LandingPage = (props) => {
console.log("PROPS TOKEN", props.token)

let content = ""

if(!props.token) {
	content = (
		<div className="d-flex container-fluid max-height row no-gutters align-items-center">
			<Col md="7" className="container-fluid ">
				<div className="img-overlay">
						<p className="text-white"><i class="fas fa-dog"></i>   &nbsp;i gib you lab</p>
						<p className="text-white"><i class="far fa-star"></i>   &nbsp;you gib me lab</p>
						<p className="text-white"><i class="fas fa-donate"></i>   &nbsp;halp other puppers</p>
				</div>
				<img src={Image01} className="img-size img-position"/>
			</Col>
			<Col md="5" className="container-fluid mt-n5">
					<Col md="12" className="">
						<RegisterPage/>
					</Col>
			</Col>
		</div>

	)
} else {
	content = (
		<Container className="bg-primary">
				<div className="bg-secondary">
					<Row className="container-fluid">
						<h1 className="text-white text-center">HOW ARE YOU FEELING TODAY?</h1>
					</Row>
					<Row className="">
							<Col className="mb-3 d-flex justify-content-center">
								<Link to="/happy" className="btn btn-primary mr-2">Happy</Link>
								<Link to="" className="btn btn-warning mr-2">Lonely</Link>
								<Link to="" className="btn btn-info ">Grievious</Link>
							</Col>
					</Row>
				</div>
		</Container>
	)
}

	return (
		<Fragment>
		{ content }
		</Fragment>
	)
}

export default LandingPage;