import React, { Fragment, useState, useEffect } from 'react';
import { Button, Container, Row, Col } from 'reactstrap';
import { Link, Redirect } from 'react-router-dom';
import axios from 'axios';
import Swal from 'sweetalert2';

//NESTING PAGES
import HappyDogsForm from '../forms/HappyDogsForm';
import HappyDogsTable from '../tables/HappyDogsTable';
// import MemberModal from '../modals/MemberModal';

const HappyDogsPage = (props) => {

  const [happyDogsData, setHappyDogsData] = useState({
    token: props.token,
    dogs: []
  })

  const { dogs } = happyDogsData;

    const url = "http://localhost:8000"

  const getDogs = async () => {
    try {
      const res = await axios.get(`${url}/dogs`)

      console.log("DOGS RESPONSE", res)

      return setHappyDogsData({
        dogs: res.data
      })
      console.log("DOGS DATA", dogs)
    } catch(e) {
      console.log(e.response)
          //SWAL
          Swal.fire({
            title: "Error",
            text: "No dogs found",
            icon: "error",
            showConfirmationButton: false,
            timer: 3000
          })
    }
  }

  useEffect(() => {
    getDogs()
  }, [setHappyDogsData])

  // const deleteMember = async (id) => {
  //   try {

  //     const res = await axios.delete(`${url}/members/${id}`, config)

  //     getMembers()
  //   } catch(e) {
  //     // SWAL
  //     Swal.fire({
  //       title: "Error",
  //       text: "Unable to delete member",
  //       icon: "error",
  //       showConfirmationButton: false,
  //       timer: 3000
  //     })
  //     // console.log(e.response)
  //   }
  // }

  //GET MEMBERS
  // useEffect(() => {
  //   getMembers()
  // }, [setMembersData]);

  //UPDATE MEMBER: MODAL
  //USE STATE
  // const [modalData, setModalData] = useState({
  //   modal: false,
  //   member: {}
  // })

  // const { modal, member } = modalData;

 //  const toggle = async (id) => {
 //    console.log(id);
 //   // alert(id);

 //   try {
 //      if(typeof id === "string") {
 //        const res = await axios.get(`${url}/members/${id}`, config)
 //        console.log(res)

 //        return setModalData({
 //          modal: !modal,
 //          member: res.data
 //        })
 //      }
 //      setModalData({
 //        ...modalData,
 //        modal: !modal
 //      })
 //   } catch(e) {
 //      console.log(e)
 //   }
 // }

  // useEffect(() => {

  // }, [toggle])

  //GET ALL TEAMS
  // const [teams, setTeams] = useState([]) //NO NEED TO DESTRUCTURE SINCE NO KEY VALUE PAIRS

  // const getTeams = async () => {
  //   try {
  //     const res = await axios.get(`${url}/teams`, config)
  //     setTeams(res.data)

  //   } catch(e) {
  //     console.log(e)
  //   }
  // }

  // useEffect(() => {
  //   getTeams()
  // }, [setTeams]) //USED WHEN YOU WANT TO DISPLAY DATA UPON LOAD


  //MEMBER PAGE
  // const updateMember = async (id, updates) => {
  //   console.log("UPDATE MEMBER", id, updates)

  //   try {
  //       config.headers["Content-Type"] = "application/json";

  //       const body = JSON.stringify(updates);

  //       const res = await axios.patch(`${url}/members/${id}`, body, config)

  //       setModalData({
  //         ...modalData,
  //         modal: !modal
  //       })

  //       getMembers()
  //       //SWAL

  //   } catch(e) {
  //     console.log(e)
  //   }
  // }

  return (
  	 <Fragment>
      <Container>
        <Row className="mb-4">
          <Col>
            <h1 className="text-black mt-5"> HAPPY DOGS </h1>
          </Col>
        </Row>      
        <Row>
          <Col md="4" className="border rounded">
          	<HappyDogsForm/>
          </Col>
          <Col md="8" className="">
              <div>
                <Button className="btn-sm border mr-1">Get All</Button>
                <Button className="btn-sm border mr-1">Get Active</Button>
                <Button className="btn-sm border mr-1">Get Deactived</Button>
              </div>
              <hr/>
              <HappyDogsTable token={props.token} dogs={dogs}/>
          </Col>
        </Row>
      </Container>
    </Fragment>
  );
}

export default HappyDogsPage;