import React, { Fragment, useState, useEffect } from 'react';
import {
	Container, Row, Col, Button, Form, FormGroup,
	Card, CardImg, CardText, CardBody, CardLink, CardTitle, CardSubtitle
} from 'reactstrap';
import StarRating from '../components/StarRating';
import axios from 'axios';
import Swal from 'sweetalert2';

import LonelyCard from '../cards/LonelyCard';
import LonelyModal from '../modals/LonelyModal'

const LonelyPage = (props) => {

	const [dogs, setDogs] = useState([])

  	const url = "http://localhost:8000"

	const getDogs = async () => {
		try {
			const res = await axios.get(`${url}/lonelydogs`)

			console.log("DOGS RESPONSE", res)

		return setDogs(res.data)
			console.log("DOGS DATA", dogs)
		} catch(e) {
			console.log(e.response)
	      	//SWAL
		      Swal.fire({
		        title: "Error",
		        text: "No dogs found",
		        icon: "error",
		        showConfirmationButton: false,
		        timer: 3000
		      })
		}
	}

	useEffect(() => {
		getDogs()
	}, [setDogs])

	//RATE MODAL
	//USESTATE
	const [modalData,  setModalData] = useState({
		modal: false,
		dog: {}
	})

	const { modal, dog } = modalData;

	//TOGGLE FUNCTION
	const toggle = async (id) => {
		console.log(id);
		// alert(id);

		try {	
			if(typeof id === "string") {
				const res = await axios.get(`${url}/lonelydogs/${id}`)

				return setModalData({
					modal: !modal,
					dog: res.data
				})
				console.log("GET DOG", res)
			}
			setModalData({
				...modalData,
				modal: !modal
			})
		} catch(e) {
			console.log(e)
		}
	}

	//USE EFFECT
	useEffect(() => {

	}, [toggle])

	//UPDATE RATE
	const updateDog = async (id, ratingValue) => {

		try {
			const config = {
				headers: {
					"Content-Type" : "application/json"
				}
			}

			const body = JSON.stringify(ratingValue);

			const res = await axios.patch(`${url}/worrydogs/${id}`, body, config)

			setModalData({
				...modalData,
				modal: !modal
			})

			getDogs()

		} catch(e) {
			console.log(e)
		}
	}
	
	return (
		<Fragment>
			<div className="bg-home02 no-gutters">
				<row className="">
					<Col md="4" className="offset-md-4 pt-5">
						<LonelyCard dogs={dogs} toggle={toggle}/>
					</Col>
				</row>
			</div>
			<LonelyModal modal={modal} toggle={toggle} dog={dog} updateDog={updateDog} />
		</Fragment>
	)
}

export default LonelyPage;