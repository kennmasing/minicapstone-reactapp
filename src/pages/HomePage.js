import React, { useState, useEffect } from 'react';
import {
	Container, Row, Col, Button,
	Card, CardImg, CardText, CardBody, CardLink, CardTitle, CardSubtitle
} from 'reactstrap';
import { Link, Redirect } from 'react-router-dom';

const HomePage = (props) => {


	return (
		<div className="container-fluid d-flex justify-content-center align-items-center max-height bg-home">
			<div className="border-rounded mt-n5">
				<h1 className="text-white text-center home-hdr mb-3">HOW ARE YOU FEELING TODAY?</h1>
				<Row className="">
						<Col className="mb-3 d-flex justify-content-center row no-gutters">
							<Link to="/happy" className="btn btn-warning mr-3 rounded-pill btn-wid"><p className="home-btn mt-3">HAPPY</p></Link>
							<Link to="/lonely" className="btn btn-primary mr-3 home-btn rounded-pill btn-wid"><p className="home-btn mt-3">LONELY</p></Link>
							<Link to="/grief" className="btn btn-info home-btn mr-3 rounded-pill btn-wid"><p className="home-btn mt-3">GRIEVOUS</p></Link>
							<Link to="/worry" className="btn btn-secondary home-btn mr-3 rounded-pill btn-wid"><p className="home-btn mt-3">ANXIOUS</p></Link>
							<Link to="/abandon" className="btn btn-success home-btn rounded-pill btn-wid"><p className="home-btn mt-3">IN LIMBO</p></Link>
						</Col>
				</Row>
			</div>
		</div>
	)
}

export default HomePage;