//IMPORT DEPENDENCIES
import React, { useState } from 'react';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import "./style.css";
import Swal from 'sweetalert2';

//IMPORT PAGES
import AppNavbar from './partials/AppNavbar';
import RegisterPage from './pages/RegisterPage';
import LoginPage from './pages/LoginPage';
import HomePage from './pages/HomePage';
import LandingPage from './pages/LandingPage';
import HappyDogProfilePage from './pages/HappyDogProfilePage';
import HappyPage from './pages/HappyPage';
import LonelyPage from './pages/LonelyPage';
import GriefPage from './pages/GriefPage';
import WorryPage from './pages/WorryPage';
import AbandonPage from './pages/AbandonPage';
import HappyDogsPage from './pages/HappyDogsPage';
import NotFoundPage from './pages/NotFoundPage';

//CONST APP
const App = () => {

	//DECLARE STATE
	const [appData, setAppData] = useState({
		token: localStorage.token,
		_id: localStorage._id,
		roleId: localStorage.roleId,
		firstName: localStorage.firstName,
		lastName: localStorage.lastName,
		username: localStorage.username,
		email: localStorage.email
	})

	//DESTRUCTURE APP DATA
	const { token, _id, roleId, firstName, lastName, username, email } = appData;

	//CREATE A FUNCTION FOR CURRENT USER
	const getCurrentUser = () => {
		return { token, _id, roleId, firstName, lastName, username, email }
	}

	//LOAD parameters(attr to be loaded, destination)
	const Load = (props, page) => {

		//LANDING PAGE
		if(page === "LandingPage") return <LandingPage {...props}/>

		//LOGIN PAGE
		if(page === "LoginPage" && token) {
			return <Redirect to="/" />
		} else if(page === "LoginPage") {
			return <LoginPage {...props} />
		}

		//REGISTER PAGE
		if(page === "RegisterPage" && token) {
			return <Redirect to="/" />
		} else if(page === "RegisterPage") {
			return <RegisterPage {...props} />
		}

		if(!token) return <Redirect to="/login" />

		if( page === "LogoutPage" ) {
			localStorage.clear()
			setAppData({
				token,
				username
			})

			Swal.fire({
	          title: "Success",
	          text: "Would you like to donate before logging out?",
	          icon: "success",
	          showConfirmationButton: false,
	          timer: 12000
	        })

			return window.location = "/login"
		}



		switch(page) {
			// case "RegisterPage": return <RegisterPage {...props} token={token} />
			case "HomePage": return <HomePage {...props} token={token} />
			// case "LandingPage": return <LandingPage {...props} token={token} />
			case "HappyDogsPage": return <HappyDogsPage {...props} token={token} />
			case "HappyDogProfilePage": return <HappyDogProfilePage {...props} token={token} />
			case "HappyPage": return <HappyPage {...props} token={token} />
			case "LonelyPage": return <LonelyPage {...props} token={token} />
			case "GriefPage": return <GriefPage {...props} token={token} />
			case "WorryPage": return <WorryPage {...props} token={token} />
			case "AbandonPage": return <AbandonPage {...props} token={token} />
			default:  return <NotFoundPage/>
		}
	}

	let navbar = ""

	if(!token) {

	} else {
		navbar = (
				<AppNavbar token={token} currentUser={firstName} roleId={roleId} className="bg-secondary"/>
		)
	}

	return (
		<BrowserRouter>
			{ navbar }
				<Switch>
					<Route exact path="/" render={ (props)=> Load(props, "LandingPage") } />
					<Route exact path="/home" render={ (props)=> Load(props, "HomePage") } />
					<Route path="/happydogs/:id" render={ (props)=> Load(props, "HappyDogProfilePage") } />
					<Route path="/happydogs" render={ (props)=> Load(props, "HappyDogsPage") } />
					<Route path="/happy" render={ (props)=> Load(props, "HappyPage") } />
					<Route path="/lonely" render={ (props)=> Load(props, "LonelyPage") } />
					<Route path="/grief" render={ (props)=> Load(props, "GriefPage") } />
					<Route path="/worry" render={ (props)=> Load(props, "WorryPage") } />
					<Route path="/abandon" render={ (props)=> Load(props, "AbandonPage") } />
					<Route path="/logout" render={ (props)=> Load(props, "LogoutPage") } />
					<Route path="/register" render={ (props)=> Load(props, "RegisterPage") } />
					<Route path="/login" render={ (props)=> Load(props, "LoginPage") } />
					<Route path="*" render={ (props)=> Load(props, "NotFoundPage") } />
				</Switch>
		</BrowserRouter>
	)

}

//EXPORT FUNCTION
export default App;