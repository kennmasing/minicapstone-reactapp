import React, { useState, useEffect } from 'react';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import { Link, Redirect } from 'react-router-dom';
import axios from 'axios';
import Swal from  'sweetalert2';

const RegisterForm = (props) => {

  const [formData, setFormData] = useState({
    username: "",
    email: "",
    password: "",
    password2: ""
  })

  const [disabledBtn, setDisabledBtn] = useState(true);
  const [isRedirected, setIsRedirected] = useState(false);
  const { username, email, password, password2 } = formData;


  const onChangeHandler = e => {

    setFormData({
      ...formData,
      [e.target.name] : e.target.value
    })
  }

  const onSubmitHandler = async e => {
    e.preventDefault();

    if(password !== password2) {
      console.log("Passwords don't match!")
      Swal.fire({
        title: "Error",
        text: "Passwords don't match!",
        icon: "error",
        showConfirmationButton: false,
        timer: 3000
      })
    } else {

      console.log(formData);

      const newUser = {
        username,
        email,
        password
      }

      const url = "http://localhost:8000"

      try {
        const config = {
          headers: {
            'Content-Type' : 'application/json'
          }
        }
        const body = JSON.stringify(newUser)
        const res = await axios.post(`${url}/users`, body, config)

        console.log(res)

        Swal.fire({
          title: "Success",
          text: "Successfully created new user!",
          icon: "success",
          showConfirmationButton: false,
          timer: 3000
        })
        setIsRedirected(true)

      } catch(e) {
        Swal.fire({
          title: "Error",
          text: "Registration error",
          icon: "error",
          showConfirmationButton: false,
          timer: 3000
        })
        console.log(e)
      }

    }

  }

  useEffect(() => {
    if(username !== "" && email !== "" && password !== "" && password2 !== "") {
      setDisabledBtn(false)
    } else {
      setDisabledBtn(true)
    }
  }, [formData])

  if(isRedirected) {
    return <Redirect to="/login" /> 
  }

  return (
    <Form className="container mt-3" onSubmit={ e => onSubmitHandler(e) }>
      <FormGroup>
        <Label for="username">Username</Label>
        <Input
          type="text"
          name="username"
          id="username"
          value={username}
          onChange={ e => onChangeHandler(e) }
          //VALIDATION
          maxLength="30"
          pattern="[a-zA-Z0-9]+" //ANY CASE, ANY COMBINATION OF ALPHANUMERIC CHARACTERS
          required
        />
      </FormGroup>    
      <FormGroup>
        <Label for="email">Email</Label>
        <Input
          type="email"
          name="email"
          id="email"
          value={email}
          onChange={ e => onChangeHandler(e) }
          //VALIDATION
          required
        />
        <FormText>Use alphanumeric characters only.</FormText>
      </FormGroup>
      <FormGroup>
        <Label for="password">Password</Label>
        <Input
          type="password"
          name="password"
          id="password"
          value={password}
          onChange={ e => onChangeHandler(e) }
          //VALIDATION
          required
          minLength="5"
        />
      </FormGroup>
      <FormGroup>
        <Label for="password2">Confirm Password</Label>
        <Input
          type="password" 
          name="password2"
          id="password2" 
          value={password2}
          onChange={ e => onChangeHandler(e) }
          //VALIDATION
          required
          minLength="5"
        />
      </FormGroup>
    
      <Button
        color="primary"
        className="btn mb-3 btn-block"
        disabled={disabledBtn}>

        Register

      </Button>
      <p>
        Already have an account? <Link to="/login">Login</Link>
      </p>
    </Form>
  );
}

export default RegisterForm;