import React, { useState, useEffect } from 'react';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import axios from 'axios';
import Swal from 'sweetalert2';



const HappyDogProfileForm = (props) => {

  const { name, description, res01, res02, res03, ratingValue } = props.dog

  const [formData, setFormData] = useState({
    name2: name,
    description2: description,
    res01_2: res01,
    res02_2: res02,
    res03_2: res03,
    ratingValue2: ratingValue
  })

  const { name2, description2, res01_2, res02_2, res03_2, ratingValue2 } = formData;


  //USE STATE


  const onChangeHandler = e => {
    setFormData({
      ...formData,
      [e.target.name] : e.target.value
    })
  }

  const onSubmitHandlder = async e => {
    e.preventDefault();

      const updates = {
        name2,
        description2,
        res01_2,
        res02_2,
        res03_2,
        ratingValue2
      }

      props.updateDog(props.dog._id, updates)

  }

  return (
    <Form className="container mt-3">
      <FormGroup>
        <Label for="name">Name</Label>
        <Input
          type="text"
          name="name"
          id="name"
          value={name}
          onChange={ e => onChangeHandler(e) }
        />
      </FormGroup>
      <FormGroup>
        <Label for="description">Description</Label>
        <Input
          type="text"
          name="description"
          id="description"
          value={description}
          onChange={ e => onChangeHandler(e) }
        />
      </FormGroup>
      <FormGroup>
        <Label for="res01">Response 1</Label>
        <Input
          type="text"
          name="res01"
          id="res01"
          value={res01}
          onChange={ e => onChangeHandler(e) }
        />
      </FormGroup>
      <FormGroup>
        <Label for="res02">Response 2</Label>
        <Input
          type="text"
          name="res02"
          id="res02"
          value={res02}
          onChange={ e => onChangeHandler(e) }
        />
      </FormGroup>

      <FormGroup>
        <Label for="res03">Response 3</Label>
        <Input
          type="text"
          name="res03"
          id="res03"
          value={res03}
          onChange={ e => onChangeHandler(e) }
      	/>
      </FormGroup>

      <Button color="primary" className="btn mt-4 mb-4 btn-block">Save Changes</Button>
    </Form>
  );
}

export default HappyDogProfileForm;