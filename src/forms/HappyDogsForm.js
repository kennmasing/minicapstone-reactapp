import React, { useState, useEffect } from 'react';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import axios from 'axios';
import Swal from 'sweetalert2';


const HappyDogsForm = (props) => {
  // console.log("M E M B E R   F O R M", props.getCurrentMemberForm)

  return (
    <Form className="container mt-3">
      <FormGroup>
        <Label for="name">Name</Label>
        <Input
          type="text"
          name="name"
          id="name"
        />
      </FormGroup>
      <FormGroup>
        <Label for="description">Description</Label>
        <Input
          type="text"
          name="description"
          id="description"
        />
      </FormGroup>
      <FormGroup>
        <Label for="res01">Response 1</Label>
        <Input
          type="text"
          name="res01"
          id="res01"
        />
      </FormGroup>
      <FormGroup>
        <Label for="res02">Response 2</Label>
        <Input
          type="text"
          name="res02"
          id="res02"
        />
      </FormGroup>

      <FormGroup>
        <Label for="res03">Response 3</Label>
        <Input
          type="text"
          name="res03"
          id="res03"
      	/>
      </FormGroup>

      <Button color="primary" className="btn mt-4 mb-4 btn-block">Save Changes</Button>
    </Form>
  );
}

export default HappyDogsForm;