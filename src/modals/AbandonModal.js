import React, { useState, useEffect } from 'react';
import { Button, Form, FormGroup, Label, Input, Modal, ModalHeader, ModalBody } from 'reactstrap';
import axios from 'axios';
import Swal from 'sweetalert2';

const AbandonModal = ({toggle, modal, dog, updateDog}) => {
	
	const [dogModal, setDogModal] = useState({
		ratingValue: ""
	})

	const { ratingValue } = dogModal;

	useEffect(() => {
		setDogModal({
			ratingValue: dog.ratingValue ? dog.ratingValue : null
		})
	}, [toggle])

	//CHANGE HANDLER
	const onChangeHandler = (e) => {
		setDogModal({
			...dogModal,
			[e.target.name] : e.target.value
		})
	}

	//SUBMIT HANDLER
	const onSubmitHandler = async (e) => {
		e.preventDefault();

		//SWAL
		let response = ""

		if(ratingValue < 3) {
			response = dog.res01
		} else if(ratingValue == 3) {
			response = dog.res02
		} else if(ratingValue > 3) {
			response = dog.res03
		}

		let titleRes = ""

		if(ratingValue < 2) {
			titleRes = "Arrwrwr.."
		} else if(ratingValue == 3) {
			titleRes = "Mehrrff"
		} else if(ratingValue > 3) {
			titleRes = "Arf Arf!"
		}

		Swal.fire({
          title: titleRes,
          text: response,
          icon: "success",
          showConfirmationButton: false,
          timer: 3000
        })

		updateDog(dog._id, { ratingValue } )
	}

	return (
		<Modal isOpen={modal} toggle={toggle}>
		<ModalHeader toggle={toggle}>Rate Me</ModalHeader>
			<ModalBody>
				<Form onSubmit={ e => onSubmitHandler(e) }>
					<FormGroup>
						<Label for="ratingValue">Rate</Label>
						<Input
							type="text"
							name="ratingValue"
							id="ratingValue"
							value={ratingValue}
							onChange={ e => onChangeHandler(e) }
						/>
					</FormGroup>
					<FormGroup>
						<div className="d-flex justify-content-center">
							<Button className="btn btn-success rounded-pill mr-1">5 USD</Button>
							<Button className="btn btn-success rounded-pill mr-1">10 USD</Button>
							<Button className="btn btn-success rounded-pill mr-1">20 USD</Button>
							<Button className="btn btn-success rounded-pill mr-1">50 USD</Button>
							<Button className="btn btn-success rounded-pill mr-1">100 USD</Button>
						</div>
					</FormGroup>
					<Button className="btn btn-block btn-primary">Submit</Button>
				</Form>
			</ModalBody>
		</Modal>
	)
}

export default AbandonModal;