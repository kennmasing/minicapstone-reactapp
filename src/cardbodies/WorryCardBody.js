import React, { Fragment } from 'react';
import { 
	Button,
	Card, CardImg, CardText, CardBody, CardLink, CardTitle, CardSubtitle
} from 'reactstrap';
import StarRating from '../components/StarRating';
import axios from 'axios';

const WorryCardBody = (props) => {
	
	const { _id, name, description, ratingValue, res01, res02, res03 } = props.dog

	const url = "http://localhost:8000"

	const updateRating = async (body) => {
		try {
			console.log(body)
			const res = await axios.post(`${url}/:id`, body)
			//SWAL
		} catch(e) {
			//SWAL
			console.log(e)
		}
	}

	return (
		<Fragment>
			<Card className="mb-5">
				<CardBody>
					<CardTitle><h1 className="text-center card-hdr">{name.toUpperCase()}</h1></CardTitle>
					<CardSubtitle className=""><h5>{description}</h5></CardSubtitle>
				</CardBody>
					<img className="img-fluid w-100 row no-gutters mt-n3" src={`http://localhost:8000/worrydogs/${_id}/upload`} />
				<CardBody>
					<CardText className="text-center"><StarRating ratingValue={ratingValue} updateRating={updateRating}/></CardText>
					<Button className="ml-1 btn btn-block btn-warning" onClick={()=> props.toggle(_id)}><i class="far fa-star"></i> Rate Me!</Button>
				</CardBody>
			</Card>
		</Fragment>
		)
}

export default WorryCardBody;