import React, { Fragment } from 'react';
import { Card, CardBody, CardTitle, Container, Col, Row } from 'reactstrap';

import HappyCardBody from '../cardbodies/HappyCardBody';

const HappyCard = (props) => {
	console.log("HAPPY DOGS", props.dogs)

let cardbody = ""

if(!props.dogs) {
	cardbody = (
		<CardBody>
			<CardTitle>
				<em>The puppers are missing</em>
			</CardTitle>
		</CardBody>
	)
} else {
	let i = 0;
	cardbody = (
		props.dogs.map(dog => {
			return <HappyCardBody
				dog={dog}
				key={dog._id}
				index={++i}
				toggle={props.toggle}
			/>
		})
	)
}

	const onSubmitHandler = (e) => {
		e.preventDefault()

		return window.location = "/happydogs"
	}

	return (
		<Fragment>
				{ cardbody }
		</Fragment>
		)
}

export default HappyCard;