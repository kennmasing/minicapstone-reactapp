import React from 'react';
import { Table } from 'reactstrap';

import HappyDogsRow from '../rows/HappyDogsRow';

const HappyDogsTable = (props) => {


  let row;

  if(!props.dogs) {
    row = (
      <tr>
        <td>
          <em>No Dogs Found...</em>
        </td>
      </tr>
    )
  } else {
    let i = 0;
    row = (
        props.dogs.map(dog => {
        return <HappyDogsRow
          dog={dog}
          key={dog._id}
          index={++i}
        />
      })
    )
  }

  return (
    <Table responsive hover light borderless size="sm" className="p-4 rounded">
      <thead>
        <tr>
          <th>#</th>
          <th>name</th>
          <th>description</th>
          <th>response 01</th>
          <th>response 02</th>
          <th>response 03</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>
        { row }
      </tbody>
    </Table>
  );
}

export default HappyDogsTable;