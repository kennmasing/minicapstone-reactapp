import React, { Fragment, useState } from 'react';
import { Link } from 'react-router-dom'
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  NavbarText,
  Button
} from 'reactstrap';

import logo from '../images/slider/logo01.png'

const AppNavbar = ({token, currentUser, roleId}) => {

  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);

  let leftLinks = "";
  if(parseInt(roleId) === 2) {

  } else if(parseInt(roleId) === 1) {
    leftLinks = (
      <Fragment>
        <NavItem>
          <Link to="/happydogs" className="nav-link">Happy Dogs</Link>
        </NavItem>
        {/*<NavItem>
          <Link to="/teams" className="nav-link">Teams</Link>
        </NavItem>
        <NavItem>
           <Link to="tasks" className="nav-link">Tasks</Link>
        </NavItem>*/}
      </Fragment>
    )
  }

  let rightLinks = "";
  if(!token) {

  } else {
    rightLinks = (
     <Fragment>
         <Link to="/me" className="btn btn-outline-light">Welcome, {currentUser.charAt(0).toUpperCase()}{currentUser.slice(1)} </Link>
         <Link to="/logout" className="ml-3 btn btn-outline-light">Logout</Link>
      </Fragment>
    )
  }

  return (
    <div className="">
      <Navbar color="primary" dark expand="md" className="navbar-height">
        <NavbarBrand className="font-weight-bold ml-3 d-flex mb-n1" href="/">
          <img src={logo} className="dog-logo mr-3"/>
          <p className="nav-hdr mt-2">pupper</p>
        </NavbarBrand>
        <NavbarToggler onClick={toggle} />
          <Collapse isOpen={isOpen} navbar>
            <Nav className="mr-auto" navbar>
              { leftLinks }
            </Nav>
              { rightLinks }
          </Collapse>
      </Navbar>
    </div>
  )

}

export default AppNavbar;