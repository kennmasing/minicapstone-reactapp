import React, { Fragment } from 'react';
import { Table, Col } from 'reactstrap';
import { Button } from 'reactstrap';
import { Link } from 'react-router-dom';
import axios from 'axios';

const HappyDogsRow = (props) => {

const { _id, name, description, res01, res02, res03, ratingValue } = props.dog

  return (
    <Fragment>
        <tr>
          <th>{ props.index }</th>
          <td>{ name.toUpperCase() }</td>
          <td>{ description }</td>
          <td>{ res01 }</td>
          <td>{ res02 }</td>
          <td>{ res03 }</td>
          <td>
            <Col>
              <Link to={`/happydogs/${_id}`} className="mb-1 btn btn-info"><i className="fas fa-eye"></i></Link>
              <Button className="mb-1 btn btn-warning"><i className="fas fa-eye"></i></Button>
              <Button className="mb-1 btn btn-danger"><i className="fas fa-eye"></i></Button>
            </Col>
          </td>
        </tr>
    </Fragment>
  );
}

export default HappyDogsRow;